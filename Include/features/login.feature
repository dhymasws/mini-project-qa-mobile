
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login
Feature: Title of your feature
  I want to use this template for my feature file

  @login-success
  Scenario: Title of your scenario outline
    Given I open app
    When I type "alex@email.com" and "123123123"
    Then I clicked button login
    
  @login-wrong-pass
  Scenario: i login with wrong pass
  	Given I open app for login
  	When I input "alex@email.com" and "1231231236978"
  	Then I click button login
  	
  @login-pass-null
  Scenario: i login with null pass
  	Given I open apl for login
  	When I input field "alex@email.com" and ""
  	Then I click button login and get message
  	
	@login-email-null
  Scenario: i login with null email
  	Given I open apk for login
  	When I input column "" and "1231231236978"
  	Then I click button login and get message error

