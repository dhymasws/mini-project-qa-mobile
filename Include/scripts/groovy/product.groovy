import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class product {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I go login")
	def I_ith_name() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I entered {string} and {string}")
	public void i_typedasd(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_email_and_pass_login'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I clicking button login")
	def I_verify_thdasasdtus_in_step() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I on homepage and add item to cart")
	def i_assaddja() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_add_item'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}