import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open app")
	public void i_open_app() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Given("I open app for login")
	public void i_open_app_2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I open apl for login")
	public void i_open_aafs2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Given("I open apk for login")
	public void i_opasdp_2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I type {string} and {string}")
	public void i_type_and(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_email_and_pass_login'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input {string} and {string}")
	public void i_type_and_2(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_email_and_pass_login'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input field {string} and {string}")
	public void i_gdsype_and_2(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_email_and_pass_login'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input column {string} and {string}")
	public void i_type_asdnd_2(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_email_and_pass_login'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	
	@Then("I clicked button login")
	def I_verify_the_status_in_step() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I click button login")
	def I_verify_the_status_in_step_2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login_wrong_pass'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I click button login and get message")
	def I_verify_tfsde_status_in_step_2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login_pass_null'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I click button login and get message error")
	def I_versdghe_status_in_step_2() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login_email_null'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}